package main

import (
	"fmt"
	"strings"
	"testing"
	"time"
	"xmpp-bot/db"
)

func TestBotHelp(t *testing.T) {
	var got, want string
	got = botHelp()
	want = "available commands"
	if !strings.Contains(got, want) {
		t.Errorf("botHelp incorrect, got: %s, want: %s", got, want)
	}
	want = "bike"
	if !strings.Contains(got, want) {
		t.Errorf("botHelp incorrect, got: %s, want: %s", got, want)
	}
}

func TestProcessBikeMessage(t *testing.T) {
	db.Init(":memory:")
	defer db.Close()

	var got, want string

	got = processBikeMessage("bike 123.456", "foobar@example.com")
	want = "Recorded 123.5 km at " + time.Now().Format("2006-01-02")
	if !strings.Contains(got, want) {
		t.Errorf("ProcessBikeMessage incorrect, got: %s, want: %s.", got, want)
	}
}

func TestGetBikeReport(t *testing.T) {
	db.Init(":memory:")
	defer db.Close()

	var got, want string
	var today string = time.Now().Format("2006-01-02")

	processBikeMessage("bike 125.0", "foobar@example.com") // +125
	processBikeMessage("bike 150.0", "foobar@example.com") // +150
	processBikeMessage("bike 75.0", "foobar@example.com")  // +75

	got = getBikeReport("foobar@example.com")
	want = fmt.Sprintf(`Bike Report:
%s: +125.0 km
%s: +150.0 km
%s: +75.0 km

Total: 350.0 km
`,
		today, today, today,
	)
	if got != want {
		t.Errorf("GetBikeReport incorrect, got: %s, want: %s.", got, want)
	}
}
