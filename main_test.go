package main

import "testing"

func TestGetUserFromAccount(t *testing.T) {
	var got, want string

	got = getUserFromAccount("foobar@example.com/Conversations.asd9as")
	want = "foobar@example.com"
	if got != want {
		t.Errorf("getUserFromAccount incorrect, got: %v, want: %v.", got, want)
	}
}
