package db

import (
	"database/sql"
	"errors"
	"log"
	"sync"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

var db *sql.DB
var dbWriteLock sync.Mutex = sync.Mutex{}

func Init(path string) {
	var err error

	dbWriteLock.Lock()
	// open sql connection
	db, err = sql.Open("sqlite3", path)
	if err != nil {
		log.Fatal(err)
	}

	// initiallize database (if necessary)
	sqlStmt := `
	CREATE TABLE IF NOT EXISTS bike (
        id INTEGER PRIMARY KEY,
        user TEXT NOT NULL,
        distance REAL,
        diff REAL,
        timestamp DATETIME
    );
    CREATE TABLE IF NOT EXISTS timetracker (
        id INTEGER PRIMARY KEY,
        user TEXT NOT NULL,
        name TEXT,
        start DATETIME,
        end DATETIME,
        duration INTEGER
    );
	`
	_, err = db.Exec(sqlStmt)
	dbWriteLock.Unlock()
	if err != nil {
		log.Fatalf("SQLite Initialization error: %s\n", err)
	}
}

func Close() {
	if db == nil {
		return
	}
	db.Close()
}

type SessionEntry struct {
	id       int64 // SQLite ROW ID
	User     string
	Name     string
	Start    time.Time
	End      time.Time
	Duration time.Duration
}

func (s *SessionEntry) Store() error {
	var err error

	if s.id != 0 {
		// entry is already in the database, just update it
		dbWriteLock.Lock()
		_, err = db.Exec(
			"UPDATE timetracker SET user = ?, name = ?, start = ?, end = ?, duration = ? WHERE id = ?",
			s.User,
			s.Name,
			s.Start,
			s.End,
			int64(s.Duration),
			s.id,
		)
		dbWriteLock.Unlock()
	} else {
		// create a new entry in database
		dbWriteLock.Lock()
		_, err = db.Exec(
			"INSERT INTO timetracker (user, name, start, end, duration) VALUES (?, ?, ?, ?, ?)",
			s.User,
			s.Name,
			s.Start,
			s.End,
			int64(s.Duration),
		)
		dbWriteLock.Unlock()
	}

	return err
}

func GetLatestRunningSession(user string, name string) (*SessionEntry, error) {
	s := SessionEntry{}
	err := db.QueryRow(
		"SELECT id, user, name, start FROM timetracker WHERE user = ? AND name = ? AND end = ? ORDER BY start DESC LIMIT 1",
		user,
		name,
		time.Time{}, // null value
	).Scan(&s.id, &s.User, &s.Name, &s.Start)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	return &s, err
}

func GetLatestSession(user string, name string) (*SessionEntry, error) {
	s := SessionEntry{}
	err := db.QueryRow(
		"SELECT id, user, name, start, end, duration FROM timetracker WHERE user = ? AND name = ? ORDER BY start DESC LIMIT 1",
		user,
	).Scan(&s.id, &s.User, &s.Name, &s.Start, &s.End, &s.Duration)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	return &s, err
}

func GetAllSessions(user string, name string) (sessions []SessionEntry, err error) {
	rows, err := db.Query(
		"SELECT id, user, name, start, end, duration FROM timetracker WHERE user = ? AND name = ?",
		user,
		name,
	)
	defer rows.Close()

	for rows.Next() {
		s := SessionEntry{}
		err = rows.Scan(&s.id, &s.User, &s.Name, &s.Start, &s.End, &s.Duration)
		if err != nil {
			return nil, err
		}
		sessions = append(sessions, s)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return sessions, nil
}

type BikeEntry struct {
	id        int64 // SQLite ROW ID
	User      string
	Distance  float64 // distance input by user
	Diff      float64 // calculated difference between entries
	Timestamp time.Time
}

func (b *BikeEntry) StoreNow() error {
	b.Timestamp = time.Now()
	return b.Store()
}

func (b *BikeEntry) Store() (err error) {
	if b.id == 0 {
		// entry does not yet exist in the database, create a new one

		// the following part is technically a bit racy, but that shouldn't matter for this app

		// fetch most recent entry
		latest, err := GetLatestBikeEntry(b.User)
		if err != nil {
			if err == sql.ErrNoRows {
				// assume that there have been no previous entries
				latest = BikeEntry{}
			} else {
				log.Printf("Failed to get latest bike entry: %s", err)
				return err
			}
		}

		// calculate difference
		b.Diff = bikeDiff(latest.Distance, b.Distance)

		// insert new entry
		dbWriteLock.Lock()
		_, err = db.Exec(
			"INSERT INTO bike (user, distance, diff, timestamp) VALUES (?, ?, ?, ?)",
			b.User,
			b.Distance,
			b.Diff,
			b.Timestamp,
		)
		dbWriteLock.Unlock()
	} else {
		// TODO: update existing entry
		err = errors.New("Store updated BikeEntry not implemented")
		log.Print(err)
	}
	if err != nil {
		return err
	}

	return nil
}

func bikeDiff(old float64, new float64) float64 {
	return new
	// diff := new - old
	// if diff < 0 {
	// 	// assume reset
	// 	diff = new
	// }
	// return diff
}

func GetLatestBikeEntry(user string) (b BikeEntry, err error) {
	err = db.QueryRow(
		"SELECT id, user, distance, diff, timestamp FROM bike WHERE user = ? ORDER BY timestamp DESC LIMIT 1",
		user,
	).Scan(&b.id, &b.User, &b.Distance, &b.Diff, &b.Timestamp)

	return b, err
}

func GetAllBikeEntries(user string) (bikeEntries []BikeEntry, err error) {
	rows, err := db.Query(
		"SELECT id, user, distance, diff, timestamp FROM bike WHERE user = ?",
		user,
	)
	defer rows.Close()

	for rows.Next() {
		b := BikeEntry{}
		err = rows.Scan(&b.id, &b.User, &b.Distance, &b.Diff, &b.Timestamp)
		if err != nil {
			return nil, err
		}
		bikeEntries = append(bikeEntries, b)
	}
	err = rows.Err()
	if err != nil {
		return nil, err
	}

	return bikeEntries, nil
}

func getBikeTotal(user string) float64 {
	var sum float64
	err := db.QueryRow(
		"SELECT SUM(diff) FROM bike WHERE user = ?",
		user,
	).Scan(&sum)
	if err != nil {
		log.Printf("getBikeTotal: %s", err)
	}

	return sum
}
