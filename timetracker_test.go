package main

import (
	"fmt"
	"strings"
	"testing"
	"time"
	"xmpp-bot/db"
)

func TestProcessTimetrackerMessage(t *testing.T) {
	db.Init(":memory:")
	defer db.Close()

	var got, want string
	var today string = time.Now().Format("2006-01-02")

	got = processTimetrackerMessage("tt start foo", "foobar@example.com")
	want = "Started session foo"
	if got != want {
		t.Errorf("ProcessTimetrackerMessage incorrect, got: %v, want: %v.", got, want)
	}

	got = processTimetrackerMessage("tt start bar", "foobar@example.com")
	want = "Started session bar"
	if got != want {
		t.Errorf("ProcessTimetrackerMessage incorrect, got: %v, want: %v.", got, want)
	}

	got = processTimetrackerMessage("tt start foo", "foobar@example.com")
	want = "last session foo is still running"
	if !strings.Contains(got, want) {
		t.Errorf("ProcessTimetrackerMessage incorrect, got: %v, want: %v.", got, want)
	}

	got = processTimetrackerMessage("tt stop foo", "foobar@example.com")
	want = "Stopped session foo, lasted "
	if !strings.Contains(got, want) {
		t.Errorf("ProcessTimetrackerMessage incorrect, got: %v, want: %v.", got, want)
	}

	got = processTimetrackerMessage("tt stop foo", "foobar@example.com")
	want = "no running session for foo"
	if !strings.Contains(got, want) {
		t.Errorf("ProcessTimetrackerMessage incorrect, got: %v, want: %v.", got, want)
	}

	got = processTimetrackerMessage("tt report foo", "foobar@example.com")
	want = fmt.Sprintf(`Timetracker report for foo:
%s: 0s
`, today,
	)
	if got != want {
		t.Errorf("ProcessTimetrackerMessage incorrect, got: %v, want: %v.", got, want)
	}

}
