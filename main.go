package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"xmpp-bot/db"

	"gosrc.io/xmpp"
	"gosrc.io/xmpp/stanza"
)

func main() {
	var xmppAddress string = os.Getenv("XMPP_ADDRESS")
	var xmppDomain string = os.Getenv("XMPP_DOMAIN")
	var xmppAccount string = os.Getenv("XMPP_ACCOUNT")
	var xmppPassword string = os.Getenv("XMPP_PASSWORD")
	var sqliteDbPath string = os.Getenv("SQLITE_DB_PATH")
	if sqliteDbPath == "" {
		sqliteDbPath = "./xmpp-bot.sqlite3"
	}

	// will exit on error
	db.Init(sqliteDbPath)

	config := xmpp.Config{
		TransportConfiguration: xmpp.TransportConfiguration{
			Address: xmppAddress,
			Domain:  xmppDomain,
		},
		Jid:          xmppAccount,
		Credential:   xmpp.Password(xmppPassword),
		StreamLogger: nil, // os.Stderr
		Insecure:     false,
		// TLSConfig: tls.Config{InsecureSkipVerify: true},
	}

	router := xmpp.NewRouter()
	router.HandleFunc("message", handleMessage)

	client, err := xmpp.NewClient(&config, router, func(err error) {
		fmt.Println(err.Error())
	})
	if err != nil {
		log.Fatalf("%+v", err)
	}

	// connection manager handles reconnect policy
	cm := xmpp.NewStreamManager(client, nil)
	log.Fatal(cm.Run())
}

func handleMessage(s xmpp.Sender, p stanza.Packet) {
	msg, ok := p.(stanza.Message)
	if !ok {
		log.Printf("Ignoring packet: %T\n", p)
		return
	}

	// ignore empty message
	if msg.Body == "" {
		return
	}

	var resp string
	fmt.Fprintf(os.Stdout, "Body = %s - from = %s\n", msg.Body, msg.From)
	switch {
	case startsWith(msg.Body, "tt") || startsWith(msg.Body, "timetracker"):
		resp = processTimetrackerMessage(msg.Body, getUserFromAccount(msg.From))
	case startsWith(msg.Body, "bike"):
		resp = processBikeMessage(msg.Body, getUserFromAccount(msg.From))
	default:
		resp = botHelp()
	}

	reply := stanza.Message{Attrs: stanza.Attrs{To: msg.From}, Body: resp}
	err := s.Send(reply)
	if err != nil {
		log.Printf("%s", err)
	}

}

func startsWith(s string, p string) bool {
	return strings.HasPrefix(strings.ToLower(s), p)
}

func getUserFromAccount(account string) string {
	return strings.Split(account, "/")[0]
}

func botHelp() string {
	return `
available commands:
 * timetracker (alias: tt)
 * bike

type '[command] help' for more details
`
}
