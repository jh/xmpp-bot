module xmpp-bot

go 1.14

require (
	github.com/mattn/go-sqlite3 v1.14.4
	gosrc.io/xmpp v0.5.1-0.20200316222903-fe4c366de849
)
