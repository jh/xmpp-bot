package main

import (
	"fmt"
	"strconv"
	"strings"
	"xmpp-bot/db"
)

func bikeHelp() string {
	return `
bike help:
 bike report
 bike [DISTANCE]

DISTANCE must be a decimal number (in kilometers, but without units)
`
}

func processBikeMessage(text string, user string) string {
	tokens := strings.Split(text, " ")
	// tokens: ["bike", "DISTANCE"]
	//            0           1
	if len(tokens) <= 1 {
		return bikeHelp()
	}

	if tokens[1] == "report" {
		return getBikeReport(user)
	}

	distance, err := strconv.ParseFloat(tokens[1], 64)
	if err != nil {
		return fmt.Sprintf("Failed to parse distance '%s'", tokens[1])
	}

	b := db.BikeEntry{
		User:     user,
		Distance: distance,
	}
	err = b.StoreNow()
	if err != nil {
		return "Internal Server Error"
	}

	return fmt.Sprintf("Recorded %.1f km at %s", b.Diff, b.Timestamp.Format("2006-01-02"))
}

func getBikeReport(user string) string {
	entries, err := db.GetAllBikeEntries(user)
	if err != nil {
		return "Interal Server Error"
	}

	pretty := "Bike Report:\n"
	var total float64
	for _, e := range entries {
		total += e.Diff
		pretty += fmt.Sprintf(
			"%s: +%.1f km\n",
			e.Timestamp.Format("2006-01-02"),
			e.Diff,
		)
	}
	pretty += fmt.Sprintf("\nTotal: %.1f km\n", total)

	return pretty
}
