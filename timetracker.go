package main

import (
	"fmt"
	"log"
	"sort"
	"strings"
	"time"
	"xmpp-bot/db"
)

func timetrackerHelp() string {
	return `
timetracker help:
 tt [start|stop|report] SESSION

if no SESSION name is provided, the 'default' session is used)
`
}

func processTimetrackerMessage(text string, user string) string {
	tokens := strings.Split(text, " ")
	// tokens: ["tt", "start|stop|report", "SESSIONNAME"]
	//            0           1                2
	if len(tokens) < 2 {
		return timetrackerHelp()
	}

	// name parsing
	var sessionName string
	if len(tokens) > 2 {
		sessionName = tokens[2]
	} else {
		sessionName = "default"
	}

	if startsWith(tokens[1], "start") {
		// check if there is a running session
		s, err := db.GetLatestRunningSession(user, sessionName)
		if err != nil {
			log.Printf("Failed to get latest running session: %s", err)
			return "Internal Server Error"
		}
		if s != nil {
			return fmt.Sprintf("Error: last session %s is still running!", sessionName)
		}

		// create a new session
		s = &db.SessionEntry{
			User:  user,
			Name:  sessionName,
			Start: time.Now(),
		}

		// save new session to database
		err = s.Store()
		if err != nil {
			log.Printf("Failed to save session: %s", err)
			return "Internal Server Error"
		}

		return fmt.Sprintf("Started session %s", s.Name)
	} else if startsWith(tokens[1], "stop") {
		// stop the recording interval

		// check if there is a running session
		s, err := db.GetLatestRunningSession(user, sessionName)
		if err != nil {
			log.Printf("Failed to get latest running session: %s", err)
			return "Internal Server Error"
		}
		if s == nil {
			return fmt.Sprintf("Error: Currently no running session for %s!", sessionName)
		}

		// end session and calculate duration
		s.End = time.Now()
		s.Duration = s.End.Sub(s.Start)

		// save to database
		err = s.Store()
		if err != nil {
			log.Printf("Failed to store session end: %s", err)
			return "Internal Server Error"
		}

		return fmt.Sprintf("Stopped session %s, lasted %s",
			s.Name,
			s.Duration.String(), // TODO: prettier duration formatting
		)
	} else if startsWith(text, "tt report") {
		return getTTReport(user, sessionName)
	}

	// no matching item, print help text
	return timetrackerHelp()
}

func getTTReport(user string, sessionName string) string {
	sessions, err := db.GetAllSessions(user, sessionName)
	if err != nil {
		log.Printf("Failed to all sessions: %s", err)
		return "Internal Server Error"
	}

	// aggregate hours on a per-day basis
	var dailyReport map[string]time.Duration = make(map[string]time.Duration)
	for _, s := range sessions {
		if s.End.IsZero() {
			log.Printf("Skipping not finished session %s on %s", s.Name, s.Start)
			continue
		}

		var day string = s.Start.Format("2006-01-02")
		dailyReport[day] += s.Duration
	}

	pretty := fmt.Sprintf("Timetracker report for %s:\n", sessionName)

	// sort the keys and iterate over them to get a sorted map
	keys := make([]string, 0, len(dailyReport))
	for k := range dailyReport {
		keys = append(keys, k)
	}
	sort.Strings(keys)

	for _, k := range keys {
		pretty += fmt.Sprintf("%s: %s\n", k,
			dailyReport[k].Truncate(time.Minute).String(),
		)
	}

	return pretty
}
