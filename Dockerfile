FROM docker.io/alpine:3.14 AS builder

RUN apk add --no-cache go sqlite-dev

WORKDIR /build

# Download dependencies
COPY go.sum go.mod ./
RUN go mod download

# Build binary
COPY . ./
RUN export CGO_ENABLED=1 && \
    go build xmpp-bot && \
    go test ./...

FROM docker.io/alpine:3.14

RUN apk add --no-cache tini sqlite
COPY --from=builder /build/xmpp-bot /usr/local/bin/xmpp-bot

USER nobody

ENTRYPOINT ["/sbin/tini", "--"]
CMD ["/usr/local/bin/xmpp-bot"]
